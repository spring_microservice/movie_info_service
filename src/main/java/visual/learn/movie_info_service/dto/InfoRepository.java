package visual.learn.movie_info_service.dto;

import org.springframework.data.repository.CrudRepository;
import visual.learn.movie_info_service.models.Movie;

import java.util.Optional;

public interface InfoRepository extends CrudRepository<Movie,String> {

    @Override
    <S extends Movie> S save(S s);

    @Override
    <S extends Movie> Iterable<S> saveAll(Iterable<S> iterable);

    @Override
    Optional<Movie> findById(String s);

    @Override
    Iterable<Movie> findAll();

    @Override
    long count();
}
