package visual.learn.movie_info_service.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import visual.learn.movie_info_service.dto.InfoRepository;
import visual.learn.movie_info_service.models.Movie;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Service
public class MovieInfoService {

    @Autowired
    private  InfoRepository infoRepository;


    @PostConstruct
    public void addDefaultMoovies() {
        infoRepository.saveAll(
                Arrays.asList(
                        new Movie("aa11", "Sholay"),
                        new Movie("bb22", "Titanic")
                ));

    }

    public void addMovieInfo(Movie movie){
        infoRepository.save(movie);
    }

    public Movie getInfoOnMovieId(String movieId){
        return infoRepository.findById(movieId).isPresent()
                ?infoRepository.findById(movieId).get()
                :null;
    }
}
