package visual.learn.movie_info_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;
@EnableEurekaClient
@ComponentScan(basePackages = {"visual.learn.movie_info_service.services",
		"visual.learn.movie_info_service.controllers",
		"visual.learn.movie_info_service.models",
		"visual.learn.movie_info_service.dto"})
@EntityScan("visual.learn.movie_info_service.models")
@SpringBootApplication
public class MovieInfoServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovieInfoServiceApplication.class, args);
	}

}
