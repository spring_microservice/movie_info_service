package visual.learn.movie_info_service.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import visual.learn.movie_info_service.models.Movie;
import visual.learn.movie_info_service.services.MovieInfoService;

@RestController
@RequestMapping("/movie")
public class MovieInfoResource {

    @Autowired
    private MovieInfoService infoService;

    @RequestMapping("/{movieId}")
    public Movie getMovieInfo(@PathVariable("movieId") String movieId){

        return infoService.getInfoOnMovieId(movieId);
    }
}
