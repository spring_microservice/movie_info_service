### [V1.1] - Basic Skeleton

- Hardcoded Movie Informations.
- No Filtering.

------
### [V1.2] - JPA Enabled

- Add Apache Derby In memory Database
- Filtering Logic implemented.

------
### [V2.1] - Added Discovery skeleton

- added Eureka Client
- added Spring Cloud Dependency
- make the application discoverable at Eureka server.