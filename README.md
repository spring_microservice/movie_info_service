# Movie Info Service

### Application Information

- **Scope** : Takes movie id and details of the movie.
- Deals with:
  - **Input**	:	Movie ID.
  - **Output**   :   Movie Details.





### Generic Info

- Deployed at port 9002.